
LOOP = loop
RUN = run
LIB = lib.bash
PREFIX ?= /usr/local

install:
	cp ${LOOP} $(PREFIX)/bin/${LOOP}
	cp ${RUN} $(PREFIX)/bin/${RUN}
	cp ${LIB} $(PREFIX)/lib/${LIB}

uninstall:
	rm -f $(PREFIX)/bin/${LOOP}
	rm -f $(PREFIX)/bin/${RUN}
