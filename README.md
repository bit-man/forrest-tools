# forrest-tools

Tools aimed at ease and simplify program execution

# Install

## bpkg

```bash
bpkg install bit-man/forrest-tools [-g]
```

## OS style

```bash
mkdir -p /my/apps/folders
cd /my/apps/folder
git clone https://gitlab.com/bit-man/forrest-tools.git
PREFIX=/usr/local/bin make install
```